# tcc

Projeto de conclusão do curso de graduação em Ciências Contábeis.
Compara o desempenho de portfólios para o mercado de ações brasileiro, selecionados pelos métodos de classificação dos algoritmos de árvore de decisão, floresta aleatória, xgboost e rede neural.
